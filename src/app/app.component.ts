import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { _throw } from 'rxjs/observable/throw';
import { delayWhen } from 'rxjs/operators';
import { timer } from 'rxjs/observable/timer';

import { OptionEntry } from './od-autocomplete/types';

@Component({
  selector: 'od-root',
  template: `
  <form class="example-form">
    <mat-form-field class="example-full-width">
      <input type="text" placeholder="Pick one" aria-label="Number" matInput [formControl]="builtIn" [matAutocomplete]="auto">
      <mat-autocomplete #auto="matAutocomplete">
        <mat-option *ngFor="let option of options" [value]="option">
          {{ option }}
        </mat-option>
      </mat-autocomplete>
    </mat-form-field>
    {{builtIn.value}}
    <hr>

    <div>Result: {{ours.value}}</div>
    <div>
      <input #manual type="text" placeholder="Change Our Value" (change)="ours.setValue(manual.value)">
    </div>
    <div style="padding: 12px; border: 1px solid">
      <od-autocomplete
      placeholder="Search People"
      [formControl]="ours"
      [displayValueFn]="valueToDisplay"
      [searchFn]="searchFn"
      ></od-autocomplete>
      </div>
      </form>
      `,
  styles: []
})
export class AppComponent {
  builtIn: FormControl = new FormControl();
  ours: FormControl = new FormControl();
  searchFn = this.search.bind(this);

  options = [
    {
      display: 'One',
      value: 1
    }, {
      display: 'Twooth',
      value: 987634
    },
    {
      display: 'Two',
      value: 2
    },
    {
      display: 'Three',
      value: 3
    }
  ];


  valueToDisplay(value: any): Observable<OptionEntry> {
    const display = value ? value + '!' : '';
    return of({
      display,
      match: true,
      value
    });
  }

  search(term: string): Observable<OptionEntry[]> {
    if (term === 'error') {
      return _throw('testing');
    }
    return of(this.options
      .filter(option => option.display.toLowerCase().indexOf(term.toLowerCase()) >= 0)
      .map(option => ({ ...option, match: option.display === term }))
    ).pipe(delayWhen(_event =>
      timer(Math.random() * 1000)
    ));
  }

}
